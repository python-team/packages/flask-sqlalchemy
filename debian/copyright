Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: flask-sqlalchemy
Source: https://github.com/pallets-eco/flask-sqlalchemy

Files: *
Copyright: 2010-2023 Armin Ronacher <armin.ronacher@active-4.com>
 2019-2023 David Lord <davidism@gmail.com>
 2019 Randy Syring <randy@thesyrings.us>
 2022-2023 Pallets <contact@palletsprojects.com>
License: BSD-3-Clause

Files: src/flask_sqlalchemy/__init__.py
Copyright: 2012 Armin Ronacher
 2012 Daniel Neuhäuser
License: BSD-3-Clause

Files: debian/*
Copyright: 2013 Thomas Bechtold <thomasbechtold@jpberlin.de>
 2021-2024 Carsten Schoenert <c.schoenert@t-online.de>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
